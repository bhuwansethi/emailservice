<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Email Service</title>
</head>
<body id="main_body">

	<div id="form_container">

		<h1>
			<a>Email Service</a>
		</h1>
		<form id="emailForm" class="appnitro" method="post" action="sendEmail">
			<div class="form_description">
				<h2>Email Service by Bhuwan Sethi</h2>
				<p>Enter the details for Sending the Email.
				The first attempt would be done using MailGun API. 
				If it is unsuccessful the next attempt would be done using GMAIL.
				** It is mandatory to provide your username and password for using gmail service.</p>
			</div>
			<ul>

				<li><label class="description">Gmail ID</label>
					<div>
						<input id="from" name="from" class="element text medium"
							type="text" maxlength="255" value="" />
					</div></li>
					
				<li><label class="description">Gmail Username</label>
					<div>
						<input id="username" name="username" class="element text medium"
							type="text" maxlength="255" value="" />
					</div></li>
					<li><label class="description">Gmail Password</label>
					<div>
						<input id="password" name="password" class="element text medium"
							type="text" maxlength="255" value="" />
					</div></li>
				<li id="li_2"><label class="description">Email
						To</label>
					<div>
						<input id="to" name="to" class="element text medium"
							type="text" maxlength="255" value="" />
					</div></li>
				<li id="li_3"><label class="description">Email
						Subject </label>
					<div>
						<textarea id="subject" name="subject"
							class="element textarea medium"></textarea>
					</div> <label class="description">Email Body </label>
					<div>
						<textarea id="body" name="body"
							class="element textarea medium"></textarea>
					</div></li>

				<li class="buttons">
				<input id="saveForm" class="button_text"
					type="submit" name="submit" value="Submit" /></li>
			</ul>
		</form>
		<div id="footer">Thank for using our Email Service. This Email
			Service is powered by MailGun and Gmail.</div>
	</div>
</body>
</html>