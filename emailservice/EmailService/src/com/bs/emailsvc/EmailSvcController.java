package com.bs.emailsvc;

import java.util.Properties;

import javax.mail.AuthenticationFailedException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

@Controller
//@RequestMapping("/index")
public class EmailSvcController {
	/*@RequestMapping(method = RequestMethod.GET)public String printHello(ModelMap model) {
	      model.addAttribute("message", "Hello Spring MVC Framework!");
	      return "index";
	   }*/
	
	@RequestMapping(value= "/sendEmail", method = RequestMethod.POST)
	public String sendEmail(ModelMap model, @RequestParam(value="to", required=true) String to, 
			@RequestParam(value="subject", required=true) String subject, 
			@RequestParam(value="body", required=true) String body, 
			@RequestParam(value="username", required=false) String username, 
			@RequestParam(value="password", required=false) String password){
		ClientResponse response;
		Client client = Client.create();
	    client.addFilter(new HTTPBasicAuthFilter("api", "key-c6ee11e0dd0badb912a24f01ea9b8440"));
	    WebResource webResource = client.resource("https://api.mailgun.net/v3/sandboxb4b44b3e79ea44a08c2ca86d68392c13.mailgun.org/messages");
	    MultivaluedMapImpl formData = new MultivaluedMapImpl();
	    formData.add("from", "Mailgun Sandbox <postmaster@sandboxb4b44b3e79ea44a08c2ca86d68392c13.mailgun.org>");
	    formData.add("to", to);
	    formData.add("subject", subject);
	    formData.add("text", body + "\n\nSent via Email Service by Bhuwan Sethi.");
	    response = webResource.type(MediaType.APPLICATION_FORM_URLENCODED).
	        post(ClientResponse.class, formData);
	    response.setStatus(300);
		if(response.getStatus() == 200){
			model.addAttribute("message", "using MailGun Email API.");
			return "success";
		} else {
			Properties props = new Properties();
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.auth", "true");
	        props.put("mail.smtp.host", "smtp.gmail.com");
	        props.put("mail.smtp.port", "587");

	        Session session = Session.getInstance(props,
	          new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication(username, password);
	            }
	          });
	        
	        try {

	            Message message = new MimeMessage(session);
	            message.setFrom(new InternetAddress(username));
	            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(to));
	            message.setSubject(subject);
	            message.setText(body + "\n\nSent via Email Service by Bhuwan Sethi.");

	            try{
	            	Transport.send(message);
	            } catch(AuthenticationFailedException e){
	            	model.addAttribute("message", "\nFor using GMAIL you would have to disable the 2-step verification and Allow less secure apps");
	            	return "error";
	            }
	            model.addAttribute("message", "using GMAIL.");
	            return "success";

	        } catch (MessagingException e) {
	            throw new RuntimeException(e);
	        }
			
		}
	    
	}
}
